package com.onlyxiahui.wofa.client.net.action;

import com.onlyxiahui.app.context.AbstractMaterial;
import com.onlyxiahui.app.context.AppContext;
import com.onlyxiahui.framework.action.dispatcher.ActionDispatcher;
import com.onlyxiahui.framework.action.dispatcher.extend.ActionResponse;
import com.onlyxiahui.framework.action.dispatcher.extend.ArgumentBox;
import com.onlyxiahui.framework.action.dispatcher.extend.impl.DefaultActionBox;
import com.onlyxiahui.framework.action.dispatcher.general.GeneralMessageHandler;
import com.onlyxiahui.framework.action.dispatcher.general.impl.GeneralArgumentActionResolver;
import com.onlyxiahui.framework.action.dispatcher.general.impl.GeneralMethodArgumentResolver;
import com.onlyxiahui.wofa.client.net.action.extend.WofaActionDispatcher;
import com.onlyxiahui.wofa.client.net.action.extend.WofaActionMessageResolverImpl;

/**
 * Description <br>
 * Date 2020-11-15 19:55:20<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class DispatcherPower extends AbstractMaterial {

	ActionDispatcher actionDispatcher = new WofaActionDispatcher();
	GeneralMessageHandler generalMessageHandler = new GeneralMessageHandler();

	public DispatcherPower(AppContext appContext) {
		super(appContext);
		initialize();
	}

	private void initialize() {
		WofaActionMessageResolverImpl amr = new WofaActionMessageResolverImpl();
		generalMessageHandler.setActionMessageResolver(amr);
		generalMessageHandler.setActionDispatcher(actionDispatcher);

		actionDispatcher.getActionBoxRegistry().add(new GeneralActionBox());
		actionDispatcher.getMethodArgumentResolverRegistry().add(new GeneralMethodArgumentResolver());
		actionDispatcher.getMethodArgumentResolverRegistry().add(new GeneralArgumentActionResolver());
	}

	public Object doMessage(Object message, ActionResponse actionResponse) {
		ArgumentBox argumentBox = generalMessageHandler.getArgumentBox();
		return generalMessageHandler.doMessage("", message, actionResponse, argumentBox);
	}

	class GeneralActionBox extends DefaultActionBox {

		@Override
		public Object getAction(Class<?> type) {

			Object o = null;
			if (null != appContext) {
				o = appContext.getObject(type);
			}
			return o;
		}
	}

	public ActionDispatcher getActionDispatcher() {
		return actionDispatcher;
	}
}

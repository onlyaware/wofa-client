
package com.onlyxiahui.wofa.client.net.action.extend;

import java.util.Set;

import com.onlyxiahui.framework.action.dispatcher.ActionContext;
import com.onlyxiahui.framework.action.dispatcher.ActionDispatcher;
import com.onlyxiahui.framework.action.dispatcher.annotation.ActionMapping;
import com.onlyxiahui.framework.action.dispatcher.util.ClassScaner;
import com.onlyxiahui.wofa.client.net.send.annotation.Sender;

/**
 * Description <br>
 * Date 2021-03-14 22:53:19<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class WofaActionContext extends ActionContext {

	public WofaActionContext(ActionDispatcher actionDispatcher) {
		super(actionDispatcher);
	}

	@SuppressWarnings("unchecked")
	public void scan(String... path) {
		// 扫描包下面的所有被注解ActionMapping的类
		Set<Class<?>> classSet = ClassScaner.scan(path, ActionMapping.class);
		for (Class<?> classType : classSet) {
			if (null == classType.getAnnotation(Sender.class)) {
				this.getActionRegistry().add(classType);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void cover(String... path) {
		// 扫描xxx包下面的所有被注解ActionMapping的类
		Set<Class<?>> classSet = ClassScaner.scan(path, ActionMapping.class);
		for (Class<?> classType : classSet) {
			if (null == classType.getAnnotation(Sender.class)) {
				this.getActionRegistry().cover(classType);
			}
		}
	}
}

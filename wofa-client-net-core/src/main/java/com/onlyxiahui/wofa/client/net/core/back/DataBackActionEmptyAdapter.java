
package com.onlyxiahui.wofa.client.net.core.back;

import com.onlyxiahui.framework.net.handler.data.action.DataBackAction;

/**
 * Description <br>
 * Date 2021-03-11 16:15:25<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public abstract class DataBackActionEmptyAdapter implements DataBackAction {

	@Override
	public void lost() {

	}

	@Override
	public void timeOut() {

	}
}
